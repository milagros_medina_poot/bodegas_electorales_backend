const jwt = require("jsonwebtoken")
const express = require("express");
const app = express();
const cors = require('cors')
app.use(cors());

const connection = require("../models/db")

const login = (req, res) => {
    const {usuario, password} = req.body
    console.log(usuario)
    console.log(password)

    const consulta = "SELECT * FROM usuarios WHERE usuario = ? AND password = ?"

    try {
        connection.query(consulta, [usuario, password], (err, result)=>{
            if(err){
                res.send(err)
            }

            if(result.length > 0){
                const token = jwt.sign({usuario}, "Stack", {expiresIn: "1d"})
                let data = {...result[0], token}

                console.log(data)
                res.send(data)
            } else {
                console.log("Usuario incorrecto")
                res.send({message: "Usuario incorrecto"})
            }
        })
    } catch (error) {
        
    }
}

app.route("/login")
.post(login)

module.exports = app