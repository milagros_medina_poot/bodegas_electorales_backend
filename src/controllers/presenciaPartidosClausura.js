const express = require("express");
const app = express();
const cors = require('cors')
app.use(cors());

const connection = require("../models/db")

const getData = (req, res) => {

    const consulta = "SELECT * FROM presencia_partidos_clausura JOIN usuarios ON presencia_partidos_clausura.id_usuario = usuarios.id JOIN casillas ON presencia_partidos_clausura.casilla_id = casillas.id;"

    try {
        connection.query(consulta, (err, result)=>{
            if(err){
                res.send(err)
            }

            if(result.length > 0){
                console.log(result)
                res.send(result)
            }
            if(result.length == 0){
                console.log("Sin registros")
                res.send("Sin regitros")
            }
        })
    } catch (error) {
        
    }
}

app.route("/presencia_partidos_clausura")
.get(getData)


const postData = (request, response) => {
    const {id_usuario, casilla_id, id_partido1, partido_politico1, presencia_representante1, id_partido2, partido_politico2, presencia_representante2, id_partido3, partido_politico3, presencia_representante3, id_partido4, partido_politico4, presencia_representante4, id_partido5, partido_politico5, presencia_representante5, id_partido6, partido_politico6, presencia_representante6, id_partido7, partido_politico7, presencia_representante7, id_partido8, partido_politico8, presencia_representante8, id_partido9, partido_politico9, presencia_representante9} = request.body;
    connection.query("INSERT INTO presencia_partidos_clausura(id_usuario, casilla_id, id_partido1, partido_politico1, presencia_representante1, id_partido2, partido_politico2, presencia_representante2, id_partido3, partido_politico3, presencia_representante3, id_partido4, partido_politico4, presencia_representante4, id_partido5, partido_politico5, presencia_representante5, id_partido6, partido_politico6, presencia_representante6, id_partido7, partido_politico7, presencia_representante7, id_partido8, partido_politico8, presencia_representante8, id_partido9, partido_politico9, presencia_representante9) VALUES (?,?,?,?,?,?,?,?,?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?, ?,?,?,?,?,?,?,?,?) ", 
    [id_usuario, casilla_id, id_partido1, partido_politico1, presencia_representante1, id_partido2, partido_politico2, presencia_representante2, id_partido3, partido_politico3, presencia_representante3, id_partido4, partido_politico4, presencia_representante4, id_partido5, partido_politico5, presencia_representante5, id_partido6, partido_politico6, presencia_representante6, id_partido7, partido_politico7, presencia_representante7, id_partido8, partido_politico8, presencia_representante8, id_partido9, partido_politico9, presencia_representante9],
    (error, results) => {
        if(error)
            throw error;
        response.status(201).json({"Item añadido correctamente": results.affectedRows});
    });
};

//ruta
app.route("/registrar_presencia_partidos_clausura")
.post(postData);

const getDataByIdUsuario = (request, response) => {
    const {id_usuario} = request.body;
    connection.query("SELECT * FROM usuarios, presencia_partidos_clausura INNER JOIN casillas ON presencia_partidos_clausura.casilla_id = casillas.id WHERE usuarios.id = (?) AND usuarios.id = presencia_partidos_clausura.id_usuario;", 
    [id_usuario],
    (error, results) => {
        if(error)
            throw error;
        response.send(results)
    });
};

//ruta
app.route("/presencia_partidos_clausura_byidusuario")
.post(getDataByIdUsuario);

module.exports = app