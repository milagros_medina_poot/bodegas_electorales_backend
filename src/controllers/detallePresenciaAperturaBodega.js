const express = require("express");
const app = express();
const cors = require('cors')
app.use(cors());

const connection = require("../models/db")

const getData = (req, res) => {

    const consulta = "SELECT * FROM detalle_presencia_apertura_bodega INNER JOIN  usuarios ON detalle_presencia_apertura_bodega.id_usuario = usuarios.id"

    try {
        connection.query(consulta, (err, result)=>{
            if(err){
                res.send(err)
            }

            if(result.length > 0){
                console.log(result)
                res.send(result)
            }

            if(result.length == 0){
                console.log("Sin registros")
                res.send("Sin regitros")
            }

        })
    } catch (error) {
        
    }
}

app.route("/detalle_presencia_apertura_bodega")
.get(getData)


const postData = (request, response) => {
    const {id_usuario, id_apertura, id_detalle_presencia, id_partido_politico, partido_politico, tipo_puesto, nombre_representante} = request.body;
    connection.query("INSERT INTO detalle_presencia_apertura_bodega(id_usuario, id_apertura, id_detalle_presencia, id_partido_politico, partido_politico, tipo_puesto, nombre_representante) VALUES (?,?,?,?,?,?,?) ", 
    [id_usuario, id_apertura, id_detalle_presencia, id_partido_politico, partido_politico, tipo_puesto, nombre_representante],
    (error, results) => {
        if(error)
            throw error;
        response.status(201).json({"Item añadido correctamente": results.affectedRows});
    });
};

//ruta
app.route("/registrar_detalle_presencia_apertura_bodega")
.post(postData);

const getDataByIdUsuario = (request, response) => {
    const {id_usuario} = request.body;
    connection.query("SELECT * FROM usuarios, detalle_presencia_apertura_bodega WHERE usuarios.id = (?) AND usuarios.id = detalle_presencia_apertura_bodega.id_usuario;", 
    [id_usuario],
    (error, results) => {
        if(error)
            throw error;
        response.send(results)
    });
};

//ruta
app.route("/detalle_presencia_apertura_bodega_byidusuario")
.post(getDataByIdUsuario);

const getIdAperturaList = (request, response) => {
    const {id_usuario} = request.body;
    connection.query("SELECT id_apertura FROM apertura_cierre WHERE id_usuario = (?);", 
    [id_usuario],
    (error, results) => {
        if(error)
            throw error;
        response.send(results)
    });
};

//ruta
app.route("/get_id_apertura_list")
.post(getIdAperturaList);


const getPartidosPoliticos = (req, res) => {

    const consulta = "SELECT id_partido_politico, partido_politico FROM partidos_politicos"

    try {
        connection.query(consulta, (err, result)=>{
            if(err){
                res.send(err)
            }

            if(result.length > 0){
                console.log(result)
                res.send(result)
            }

            if(result.length == 0){
                console.log("Sin registros")
                res.send("Sin regitros")
            }

        })
    } catch (error) {
        
    }
}

app.route("/partidos_politicos")
.get(getPartidosPoliticos)

module.exports = app 