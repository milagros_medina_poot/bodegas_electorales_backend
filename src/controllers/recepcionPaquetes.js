const express = require("express");
const app = express();
const cors = require('cors')
app.use(cors());

const connection = require("../models/db")

const getData = (req, res) => {

    const consulta = "SELECT *, DATE_FORMAT(fecha_hora_recepcion, '%d/%m/%Y %H:%i') AS recepcion FROM recepcion_paquetes JOIN  usuarios ON recepcion_paquetes.id_usuario = usuarios.id JOIN casillas ON recepcion_paquetes.casilla_id = casillas.id"

    try {
        connection.query(consulta, (err, result)=>{
            if(err){
                res.send(err)
            }

            if(result.length > 0){
                console.log(result)
                res.send(result)
            }
            if(result.length == 0){
                console.log("Sin registros")
                res.send("Sin regitros")
            }
        })
    } catch (error) {
        
    }
}

app.route("/recepcion_paquetes")
.get(getData)


const postData = (request, response) => {
    const {id_usuario, casilla_id, fecha_hora_recepcion, estado_del_paquete, cargo_responsable_entrega, paquete_sellado, paq_sellado_etiqueta_seg, estatus_paquete} = request.body;
    connection.query("INSERT INTO recepcion_paquetes(id_usuario, casilla_id, fecha_hora_recepcion, estado_del_paquete, cargo_responsable_entrega, paquete_sellado, paq_sellado_etiqueta_seg, estatus_paquete) VALUES (?,?,?,?,?,?,?,?) ", 
    [id_usuario, casilla_id, fecha_hora_recepcion, estado_del_paquete, cargo_responsable_entrega, paquete_sellado, paq_sellado_etiqueta_seg, estatus_paquete],
    (error, results) => {
        if(error)
            throw error;
        response.status(201).json({"Item añadido correctamente": results.affectedRows});
    });
};

//ruta
app.route("/registrar_recepcion_paquetes")
.post(postData);

const getDataByIdUsuario = (request, response) => {
    const {id_usuario} = request.body;
    connection.query("SELECT *, DATE_FORMAT(fecha_hora_recepcion, '%d/%m/%Y %H:%i') AS recepcion FROM usuarios, recepcion_paquetes INNER JOIN casillas ON recepcion_paquetes.casilla_id = casillas.id WHERE usuarios.id = (?) AND usuarios.id = recepcion_paquetes.id_usuario;", 
    [id_usuario],
    (error, results) => {
        if(error)
            throw error;
        response.send(results)
    });
};

//ruta
app.route("/recepcion_paquetes_byidusuario")
.post(getDataByIdUsuario);

module.exports = app