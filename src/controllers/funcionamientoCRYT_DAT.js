const express = require("express");
const app = express();
const cors = require('cors')
app.use(cors());

const connection = require("../models/db")

const getData = (req, res) => {

    const consulta = "SELECT *, DATE_FORMAT(fecha_hora_apertura, '%d/%m/%Y %H:%i:%S %p') AS apertura, DATE_FORMAT(fecha_hora_cierre, '%d/%m/%Y %H:%i:%S %p') AS cierre, DATE_FORMAT(fecha_hora_inicio_custodia_fun, '%d/%m/%Y %H:%i:%S %p') AS inicio_fun, DATE_FORMAT(fecha_hora_fin_custodia_fun, '%d/%m/%Y %H:%i:%S %p') AS fin_fun, DATE_FORMAT(fecha_hora_inicio_custodia_tra, '%d/%m/%Y %H:%i:%S %p') AS inicio_tra, DATE_FORMAT(fecha_hora_fin_custodia_tra, '%d/%m/%Y %H:%i:%S %p') AS fin_tra FROM funcionamiento_cryt_dat INNER JOIN  usuarios ON funcionamiento_cryt_dat.id_usuario = usuarios.id"

    try {
        connection.query(consulta, (err, result)=>{
            if(err){
                res.send(err)
            }

            if(result.length > 0){
                console.log(result)
                res.send(result)
            }
            if(result.length == 0){
                console.log("Sin registros")
                res.send("Sin regitros")
            }
        })
    } catch (error) {
        
    }
}

app.route("/funcionamiento_cryt_dat")
.get(getData)


const postData = (request, response) => {
    const {id_usuario, id_cryt_dat, tipo_cryt_dat, fecha_hora_apertura, fecha_hora_cierre, horas_traslado, minutos_traslado, distancia_traslado_destino_final, fecha_hora_inicio_custodia_fun, fecha_hora_fin_custodia_fun, fecha_hora_inicio_custodia_tra, fecha_hora_fin_custodia_tra, incidentes_cryt_dat, soluciones_cryt_dat} = request.body;
    connection.query("INSERT INTO funcionamiento_cryt_dat(id_usuario, id_cryt_dat, tipo_cryt_dat, fecha_hora_apertura, fecha_hora_cierre, horas_traslado, minutos_traslado, distancia_traslado_destino_final, fecha_hora_inicio_custodia_fun, fecha_hora_fin_custodia_fun, fecha_hora_inicio_custodia_tra, fecha_hora_fin_custodia_tra, incidentes_cryt_dat, soluciones_cryt_dat) VALUES (?,?,?,?,?,?,?,?,?, ?, ?, ?, ?, ?) ", 
    [id_usuario, id_cryt_dat, tipo_cryt_dat, fecha_hora_apertura, fecha_hora_cierre, horas_traslado, minutos_traslado, distancia_traslado_destino_final, fecha_hora_inicio_custodia_fun, fecha_hora_fin_custodia_fun, fecha_hora_inicio_custodia_tra, fecha_hora_fin_custodia_tra, incidentes_cryt_dat, soluciones_cryt_dat],
    (error, results) => {
        if(error)
            throw error;
        response.status(201).json({"Item añadido correctamente": results.affectedRows});
    });
};

//ruta
app.route("/registrar_funcionamiento_cryt_dat")
.post(postData);

const getDataByIdUsuario = (request, response) => {
    const {id_usuario} = request.body;
    connection.query("SELECT *,DATE_FORMAT(fecha_hora_apertura, '%d/%m/%Y %H:%i:%S %p') AS apertura, DATE_FORMAT(fecha_hora_cierre, '%d/%m/%Y %H:%i:%S %p') AS cierre, DATE_FORMAT(fecha_hora_inicio_custodia_fun, '%d/%m/%Y %H:%i:%S %p') AS inicio_fun, DATE_FORMAT(fecha_hora_fin_custodia_fun, '%d/%m/%Y %H:%i:%S %p') AS fin_fun, DATE_FORMAT(fecha_hora_inicio_custodia_tra, '%d/%m/%Y %H:%i:%S %p') AS inicio_tra, DATE_FORMAT(fecha_hora_fin_custodia_tra, '%d/%m/%Y %H:%i:%S %p') AS fin_tra FROM usuarios, funcionamiento_cryt_dat WHERE usuarios.id = (?) AND usuarios.id = funcionamiento_cryt_dat.id_usuario;", 
    [id_usuario],
    (error, results) => {
        if(error)
            throw error;
        response.send(results)
    });
};

//ruta
app.route("/funcionamiento_cryt_dat_byidusuario")
.post(getDataByIdUsuario);

module.exports = app