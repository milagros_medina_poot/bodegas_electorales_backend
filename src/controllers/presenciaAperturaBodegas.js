const express = require("express");
const app = express();
const cors = require('cors')
app.use(cors());

const connection = require("../models/db")

const getData = (req, res) => {

    const consulta = "SELECT * FROM presencia_apertura_bodegas INNER JOIN  usuarios ON presencia_apertura_bodegas.id_usuario = usuarios.id"

    try {
        connection.query(consulta, (err, result)=>{
            if(err){
                res.send(err)
            }

            if(result.length > 0){
                console.log(result)
                res.send(result)
            }
            if(result.length == 0){
                console.log("Sin registros")
                res.send("Sin regitros")
            }
        })
    } catch (error) {
        
    }
}

app.route("/presencia_apertura_bodegas")
.get(getData)


const postData = (request, response) => {
    const {id_usuario, id_presencia_apertura, id_apertura, tipo_puesto, presencia_apert_bodegas, cierre_apertura} = request.body;
    connection.query("INSERT INTO presencia_apertura_bodegas(id_usuario, id_presencia_apertura, id_apertura, tipo_puesto, presencia_apert_bodegas, cierre_apertura) VALUES (?,?,?,?,?,?) ", 
    [id_usuario, id_presencia_apertura, id_apertura, tipo_puesto, presencia_apert_bodegas, cierre_apertura],
    (error, results) => {
        if(error)
            throw error;
        response.status(201).json({"Item añadido correctamente": results.affectedRows});
    });
};

//ruta
app.route("/registrar_presencia_apertura_bodegas")
.post(postData);

const getDataByIdUsuario = (request, response) => {
    const {id_usuario} = request.body;
    connection.query("SELECT * FROM usuarios, presencia_apertura_bodegas WHERE usuarios.id = (?) AND usuarios.id = presencia_apertura_bodegas.id_usuario;", 
    [id_usuario],
    (error, results) => {
        if(error)
            throw error;
        response.send(results)
    });
};

//ruta
app.route("/presencia_apertura_bodegas_byidusuario")
.post(getDataByIdUsuario);








const insertRecords = (values, callback) => {
    const sql = 'INSERT INTO presencia_apertura_bodegas (id_usuario, id_presencia_apertura, id_apertura, tipo_puesto, presencia_apert_bodegas, cierre_apertura) VALUES ?';
    connection.query(sql, [values], (err, result) => {
      if (err) {
        callback(err);
        return;
      }
      callback(null, result.affectedRows);
    });
  };


const postRegistros = (request, response) => {
    const { records } = request.body;

    const values = records.map(record => [record.id_usuario, record.id_presencia_apertura, record.id_apertura, record.tipo_puesto, record.presencia_apert_bodegas, record.cierre_apertura]);

    insertRecords(values, (err, affectedRows) => {
        if (err) {
          return response.status(500).json({ error: 'Error al insertar datos', details: err });
        }
        response.json({ message: 'Registros guardados correctamente', affectedRows });
      });
};

//ruta
app.route("/registrar_varios_presencia_apertura_bodegas")
.post(postRegistros);

module.exports = app