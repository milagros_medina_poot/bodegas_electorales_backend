const express = require("express");
const app = express();
const cors = require('cors')
app.use(cors());

const connection = require("../models/db")

const getData = (req, res) => {

    const consulta = "SELECT *, DATE_FORMAT(fecha_hora_clausura, '%d/%m/%Y %H:%i') AS clausura FROM clausura_casillas JOIN  usuarios ON clausura_casillas.id_usuario = usuarios.id JOIN casillas ON clausura_casillas.casilla_id = casillas.id"

    try {
        connection.query(consulta, (err, result)=>{
            if(err){
                res.send(err)
            }

            if(result.length > 0){
                console.log(result)
                res.send(result)
            }

            if(result.length == 0){
                console.log("Sin registros")
                res.send("Sin regitros")
            }

        })
    } catch (error) {
        
    }
}

app.route("/clausura_casillas")
.get(getData)


const postClausuraCasillas = (request, response) => {
    const {id_usuario, casilla_id, fecha_hora_clausura, presencia_presidente, presencia_secretario, presencia_segundo_secretario, presencia_primer_escrutador, presencia_segundo_escrutador, presencia_tercer_escrutador, constancia, estatus_casilla_instalada, motivo_cambio_domicilio} = request.body;
    connection.query("INSERT INTO clausura_casillas(id_usuario, casilla_id, fecha_hora_clausura, presencia_presidente, presencia_secretario, presencia_segundo_secretario, presencia_primer_escrutador, presencia_segundo_escrutador, presencia_tercer_escrutador, constancia, estatus_casilla_instalada, motivo_cambio_domicilio) VALUES (?,?,?,?,?,?,?,?,?,?,?,?) ", 
    [id_usuario, casilla_id, fecha_hora_clausura, presencia_presidente, presencia_secretario, presencia_segundo_secretario, presencia_primer_escrutador, presencia_segundo_escrutador, presencia_tercer_escrutador, constancia, estatus_casilla_instalada, motivo_cambio_domicilio],
    (error, results) => {
        if(error)
            throw error;
        response.status(201).json({"Item añadido correctamente": results.affectedRows});
    });
};

//ruta
app.route("/registrar_clausura_casillas")
.post(postClausuraCasillas);


const getDataByIdUsuario = (request, response) => {
    const {id_usuario} = request.body;
    connection.query("SELECT *, DATE_FORMAT(fecha_hora_clausura, '%d/%m/%Y %H:%i') AS clausura FROM usuarios, clausura_casillas INNER JOIN casillas ON clausura_casillas.casilla_id = casillas.id WHERE usuarios.id = (?) AND usuarios.id = clausura_casillas.id_usuario;", 
    [id_usuario],
    (error, results) => {
        if(error)
            throw error;
        response.send(results)
    });
};

//ruta
app.route("/clausura_casillas_byidusuario")
.post(getDataByIdUsuario);

module.exports = app 