const express = require("express");
const app = express();
const cors = require('cors')
app.use(cors());

const connection = require("../models/db")

const getData = (req, res) => {

    const consulta = "SELECT *, DATE_FORMAT(fecha_hora_salida, '%d/%m/%Y %H:%i') AS salida, DATE_FORMAT(fecha_hora_entrada, '%d/%m/%Y %H:%i') AS entrada FROM bitacora_paquetes_electorales JOIN  usuarios ON bitacora_paquetes_electorales.id_usuario = usuarios.id JOIN casillas ON bitacora_paquetes_electorales.casilla_id = casillas.id"

    try {
        connection.query(consulta, (err, result)=>{
            if(err){
                res.send(err)
            }

            if(result.length > 0){
                console.log(result)
                res.send(result)
            }
            if(result.length == 0){
                console.log("Sin registros")
                res.send("Sin regitros")
            }
        })
    } catch (error) {
        
    }
}

app.route("/bitacora_paquetes_electorales")
.get(getData)


const postBitacora = (request, response) => {
    const {id_usuario, casilla_id, fecha_hora_salida, fecha_hora_entrada, funcionario_entrega_salida, nombre_funcionario_entrega_salida, funcionario_recibe_salida, nombre_funcionario_recibe_salida, funcionario_entrega_entrada, nombre_funcionario_entrega_entrada, funcionario_recibe_entrada, nombre_funcionario_recibe_entrada} = request.body;
    connection.query("INSERT INTO bitacora_paquetes_electorales(id_usuario, casilla_id, fecha_hora_salida, fecha_hora_entrada, funcionario_entrega_salida, nombre_funcionario_entrega_salida, funcionario_recibe_salida, nombre_funcionario_recibe_salida, funcionario_entrega_entrada, nombre_funcionario_entrega_entrada, funcionario_recibe_entrada, nombre_funcionario_recibe_entrada) VALUES (?,?,?,?,?,?,?,?,?,?,?,?) ", 
    [id_usuario, casilla_id, fecha_hora_salida, fecha_hora_entrada, funcionario_entrega_salida, nombre_funcionario_entrega_salida, funcionario_recibe_salida, nombre_funcionario_recibe_salida, funcionario_entrega_entrada, nombre_funcionario_entrega_entrada, funcionario_recibe_entrada, nombre_funcionario_recibe_entrada],
    (error, results) => {
        if(error)
            throw error;
        response.status(201).json({"Item añadido correctamente": results.affectedRows});
    });
};

//ruta
app.route("/registrar_bitacora")
.post(postBitacora);


const getDataByIdUsuario = (request, response) => {
    const {id_usuario} = request.body;
    connection.query("SELECT *, DATE_FORMAT(fecha_hora_salida, '%d/%m/%Y %H:%i') AS salida, DATE_FORMAT(fecha_hora_entrada, '%d/%m/%Y %H:%i') AS entrada FROM usuarios, bitacora_paquetes_electorales INNER JOIN casillas ON bitacora_paquetes_electorales.casilla_id = casillas.id WHERE usuarios.id = (?) AND usuarios.id = bitacora_paquetes_electorales.id_usuario;", 
    [id_usuario],
    (error, results) => {
        if(error)
            throw error;
        response.send(results)
    });
};

//ruta
app.route("/bitacora_paquetes_byidusuario")
.post(getDataByIdUsuario);


const getCasillas = (req, res) => {

    const consulta = "SELECT * FROM casillas"

    try {
        connection.query(consulta, (err, result)=>{
            if(err){
                res.send(err)
            }

            if(result.length > 0){
                console.log(result)
                res.send(result)
            }
            if(result.length == 0){
                console.log("Sin registros")
                res.send("Sin regitros")
            }
        })
    } catch (error) {
        
    }
}

app.route("/get_casillas")
.get(getCasillas)


module.exports = app