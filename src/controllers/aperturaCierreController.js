const express = require("express");
const app = express();
const cors = require('cors')
app.use(cors());

const connection = require("../models/db")

const getData = (req, res) => {

    const consulta = "SELECT *, DATE_FORMAT(fecha_hora_apertura, '%d/%m/%Y %H:%i') AS apertura, DATE_FORMAT(fecha_hora_cierre, '%d/%m/%Y %H:%i') AS cierre FROM apertura_cierre INNER JOIN  usuarios ON apertura_cierre.id_usuario = usuarios.id"

    try {
        connection.query(consulta, (err, result)=>{
            if(err){
                res.send(err)
            }

            if(result.length > 0){
                console.log(result)
                res.send(result)
            }
            if(result.length == 0){
                console.log("Sin registros")
                res.send("Sin regitros")
            }
        })
    } catch (error) {
        
    }
}

app.route("/apertura_cierre")
.get(getData)


const postData = (request, response) => {
    const {id_usuario, id_apertura, fecha_hora_apertura, fecha_hora_cierre, cantidad_medios, cantidad_tecnicos, cantidad_te, motivo, con_sello_puerta,con_sello_ventana, observaciones, aplico_sello_puerta, aplico_sello_ventana, cantidad_medios_cierre, cantidad_tecnicos_cierre, cantidad_te_cierre} = request.body;
    connection.query("INSERT INTO apertura_cierre(id_usuario, id_apertura, fecha_hora_apertura, fecha_hora_cierre, cantidad_medios, cantidad_tecnicos, cantidad_te, motivo, con_sello_puerta,con_sello_ventana, observaciones, aplico_sello_puerta, aplico_sello_ventana, cantidad_medios_cierre, cantidad_tecnicos_cierre, cantidad_te_cierre) VALUES (?,?,?,?,?,?,?,?,?, ?, ?, ?, ?, ?, ?, ?) ", 
    [id_usuario, id_apertura, fecha_hora_apertura, fecha_hora_cierre, cantidad_medios, cantidad_tecnicos, cantidad_te, motivo, con_sello_puerta,con_sello_ventana, observaciones, aplico_sello_puerta, aplico_sello_ventana, cantidad_medios_cierre, cantidad_tecnicos_cierre, cantidad_te_cierre],
    (error, results) => {
        if(error)
            throw error;
        response.status(201).json({"Item añadido correctamente": results.affectedRows});
    });
};

//ruta
app.route("/registrar_apertura_cierre")
.post(postData);

const getDataByIdUsuario = (request, response) => {
    const {id_usuario} = request.body;
    connection.query("SELECT *, DATE_FORMAT(fecha_hora_apertura, '%d/%m/%Y %H:%i') AS apertura, DATE_FORMAT(fecha_hora_cierre, '%d/%m/%Y %H:%i') AS cierre FROM usuarios, apertura_cierre WHERE usuarios.id = (?) AND usuarios.id = apertura_cierre.id_usuario;", 
    [id_usuario],
    (error, results) => {
        if(error)
            throw error;
        response.send(results)
    });
};

//ruta
app.route("/apertura_cierre_byidusuario")
.post(getDataByIdUsuario);

module.exports = app