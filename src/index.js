const express = require("express");
const PORT = 5000;

const app = express()
const cors = require("cors")

app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(cors())

app.use(require('./controllers/loginController'));
app.use(require('./controllers/aperturaCierreController'));
app.use(require('./controllers/bitacoraPaquetesElectoralesController'));
app.use(require('./controllers/clausuraCasillasController'));
app.use(require('./controllers/detallePresenciaAperturaBodega'));
app.use(require('./controllers/funcionamientoCRYT_DAT'));
app.use(require('./controllers/presenciaAperturaBodegas'));
app.use(require('./controllers/recepcionPaquetes'));
app.use(require('./controllers/presenciaPartidosClausura'));

app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});